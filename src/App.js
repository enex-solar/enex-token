import React, { Component } from 'react'
import { BrowserRouter as Router, Route} from 'react-router-dom'
import { Web3Provider } from 'react-web3'

//Components and pages

import NavBar from './components/NavBar'
import FrontPage from  './components/FrontPage'
import Profile from  './components/Profile'
import ProjectDetails from  './components/ProjectDetails'

//Styles
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'mdbreact/docs/css/mdb.min.css';
import './App.css';

class App extends Component {

constructor(props, context) {
    super(props)
  }

render() {

  // If no MetaMask, display error

  const NoAccountDetected = () => {
    return (
    <div>
      <NavBar/>
      <main>
        <div className="main-wrapper">
          <div className="container">
            <div className="alert alert-danger alert-rounded" style={{marginTop: '50px'}}>
              <img src="https://cdn.worldvectorlogo.com/logos/metamask.svg" width="30" style={{marginRight: '10px'}}className="img-circle" alt="MetaMask logo" />
              Oops! No MetaMask detected - don't worry, just sign in to MetaMask or you can download it from <a href="http://metamask.io" className="alert-link">here</a>.
            </div>
          </div>
        </div>
      </main>
    </div>
    )
  }

  //If no Web3, display error

  const NoWeb3Detected = () => {
    return (
    <div>
      <NavBar/>
      <main>
        <div className="main-wrapper">
          <div className="container">
            <div className="alert alert-danger alert-rounded" style={{marginTop: '50px'}}>
              <img src="https://cdn.worldvectorlogo.com/logos/metamask.svg" width="30" style={{marginRight: '10px'}}className="img-circle" alt="MetaMask logo" />
              Oops! No MetaMask detected - don't worry, just sign in to MetaMask or you can download it from <a href="http://metamask.io" className="alert-link">here</a>.
            </div>
          </div>
        </div>
      </main>
    </div>
    )
  }

  return(
    <Router>
      <Web3Provider
        web3UnavailableScreen={() => <NoWeb3Detected />}
        accountUnavailableScreen={() => <NoAccountDetected />}
      >
      <div>
        <Route exact path="/" component={FrontPage}/>
        <Route path="/profile" component={Profile}/>
        <Route path="/project/:projectId" component={ProjectDetails}/>
        <NavBar/>
      </div>
      </Web3Provider>
    </Router>
    )
  }

}


export default App
