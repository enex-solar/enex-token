import React, { Component } from 'react';
import contractService from '../services/contractService'
import { Navbar, NavbarBrand, NavbarNav, NavbarToggler, Collapse, NavItem, NavLink } from 'mdbreact';
import { Link } from 'react-router-dom'

export default class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapse: false,
            isWideEnough: false,
            dropdownOpen: false
        };
    this.onClick = this.onClick.bind(this);
    this.toggle = this.toggle.bind(this);
    }

    onClick(){
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    componentDidMount() {

        //Get the balance
        this.getBalance();

    }

    getBalance() {
        contractService.getBalance()
        .then((accountBalance) => {
          this.setState({accountBalance:accountBalance})
        })
        .catch((error) => {
          console.error(error)
        })
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }
    render() {
        return (
                <Navbar color="indigo" dark expand="md" className="navbar-enex" fixed="top">
                    <div className="container">
                        <NavbarBrand href="/">
                            <img src="/logo_white.png" style={{height:'35px'}} />
                        </NavbarBrand>
                        { !this.state.isWideEnough && <NavbarToggler onClick={ this.onClick } />}
                        <Collapse isOpen={ this.state.collapse } navbar>
                            <NavbarNav className="ml-auto">
                                <NavItem>
                                    <Link className="nav-link" to="/profile"><i className="fa fa-user"/> Account - {this.state.accountBalance} ENX</Link>
                                </NavItem>
                            </NavbarNav>
                        </Collapse>
                    </div>
                </Navbar>
        );
    }
}