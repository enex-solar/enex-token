import React, { Component } from 'react';
import contractService from '../services/contractService'
import ProjectList from './ProjectList';

export default class FrontPage extends Component {
    constructor(props) {
       super(props)
    }

  componentDidMount() {

    //Initialize listener for blockhain transactions to update state
    window.web3.eth.filter('latest', (error, result) => {
      if (!error) {
        console.log(result)
        this.getBalance();
        this.getUser();
      } else {
        console.error(error)
      }
    });
  }

  componentWillUnmount() {
    //Remove listener
    window.web3.eth.filter('latest').stopWatching();
  }

  updateValue(event) {
    this.setState({ stakeAmount:Number(event.target.value) });
  }


  render() {
    return(
        <main>
            <div className="main-wrapper">
                <div className="container">
                    <div className="row mt-5">
                      <div className="col-md-12">
                        <h2 className="main-title dark-grey-color">Solar Projects</h2>
                      </div>
                    </div>
                    <div className="row">
                        <ProjectList/>
                    </div>
                </div>
            </div>
        </main>
        )
    }
}