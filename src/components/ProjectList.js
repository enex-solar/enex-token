import React, { Component } from 'react'
import ProjectCard from './ProjectCard'
import contractService from '../services/contractService'

class ProjectList extends Component {

  constructor(props, context) {
    super(props)
    this.state = {
      projectIds: []
    }
  }


  componentWillMount() {
    contractService.getProjectIds()
        .catch((error) => {
            console.log(error)
        }).then(projectids => {
        this.setState({ projectIds: projectids})
      })
  }

  render() {

    var intialLoader=null;

    if(this.state.initialLoading) {
      var initialLoader=(
      <div className="m-t-20">
        <div className="la-ball-scale-pulse">
              <div></div>
              <div></div>
        </div>
      </div>)
    } else {
      var intialLoader=null
    }


    return (
      <div>
          {initialLoader}
          {this.state.projectIds.map((projectId, i) =>
            (<div className="col-md-5 float-left mt-2" key={projectId}><ProjectCard projectId={projectId}/></div>)
          )}
      </div>
    )
  }
}

export default ProjectList