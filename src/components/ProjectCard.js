import React, { Component } from 'react'
import contractService from '../services/contractService';
import CountUp from 'react-countup';
import { Link } from 'react-router-dom'
import { Fa, Button, Card, CardBody, CardImage, CardTitle, CardText } from 'mdbreact';


class ProjectCard extends Component {

  constructor(props, context) {
    super(props)
    this.state = {
      projectName: "Loading..",
      projectVotes: null
    }
  }


  componentWillMount() {
    contractService.getProject(this.props.projectId)
        .catch((error) => {
            console.log(error)
        }).then(project => {
        this.setState({ projectName: project.name, projectVotes: project.votes})
      })
  }

  render() {
    return (
		<Card>
		    <CardImage className="img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Photovoltaik_Dachanlage_Hannover_-_Schwarze_Heide_-_1_MW.jpg/2560px-Photovoltaik_Dachanlage_Hannover_-_Schwarze_Heide_-_1_MW.jpg" />
		    <CardBody>
		        <div className="row">
                <div className="col-md-6 align-self-center">
                  <h4 style={{'font-size':'28px'}}>{this.state.projectName}</h4>
                  <p className="card-text">Description..</p>
                </div>
  		        <div className="col-md-6">
                <div className="float-right">
                  <h5 className="text-center badge enex-bg-gradient" style={{'font-size':'28px'}}><CountUp start={0} end={this.state.projectVotes} duration={0.5} /><br />
                  <div style={{'font-size':'18px'}}>Votes</div>
                  </h5>
                </div>
              </div>
            </div>
            <div className="mt-2">
		          <Link to={`/project/${this.props.projectId}`}><Button color="blue"><Fa icon="eye" className="mr-1"/>View details</Button></Link>
            </div>
		    </CardBody>
		</Card>
    )
  }
}

export default ProjectCard