import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import contractService from '../services/contractService';
import CountUp from 'react-countup';
import { Fa, Button, Card, CardBody, CardImage, CardTitle, CardText } from 'mdbreact';


class ProjectDetails extends Component {

  constructor(props, context) {
    super(props)
    this.state = {
      projectName: "Loading..",
      projectVotes: null,
      projectId: this.props.match.params.projectId
    }
    this.voteProject.bind(this)
    this.getProjectInfo.bind(this)
  }


  componentWillMount() {
    this.getProjectInfo();

   //Initialize listener for blockhain transactions to update state
    window.web3.eth.filter('latest', (error, result) => {
      if (!error) {
        console.log(result)
        this.getProjectInfo();
      } else {
        console.error(error)
      }
    });
  }

  componentWillUnmount() {
    //Remove listener
    window.web3.eth.filter('latest').stopWatching();
  }

  //Function to get project info from the blockchain
  getProjectInfo() {
    contractService.getProject(this.props.match.params.projectId)
        .catch((error) => {
            console.log(error)
        }).then(project => {
        this.setState({ projectName: project.name, projectVotes: project.votes})
      })
  }

  //Function to vote for the project
  voteProject() {
    contractService.voteProject(this.state.projectId)
        .then((transaction) => {
            console.log(transaction)
        })
        .catch((error) => {
          console.error(error)
          alert(error)
      })
  }

  render() {
    return (
      <div className="main-wrapper">
        <div className="container">
             <div className="row">
               <div className="col-md-6">
                <Card>
                  <CardBody>
                      <div className="row">
                          <div className="col-md-6 align-self-center">
                            <h4 style={{'font-size':'28px'}}>{this.state.projectName}</h4>
                          </div>
                        <div className="col-md-6 align-self-center">
                          <div className="float-right">
                            <h5 className="text-center badge enex-bg-gradient" style={{'font-size':'28px'}}><CountUp start={0} end={this.state.projectVotes} duration={0.5} /><br />
                            <div style={{'font-size':'18px'}}>Votes</div>
                            </h5>
                          </div>
                        </div>
                      </div>
                      <CardText>Project description here.</CardText>
                        <div>
                        <Button color="orange" onClick={this.voteProject.bind(this)}><Fa icon="thumbs-up" className="mr-1"/>Vote</Button>
                      </div>
                  </CardBody>
              </Card>
          </div>
          <div className="col-md-6">
             <img className="img-fluid z-depth-1" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Photovoltaik_Dachanlage_Hannover_-_Schwarze_Heide_-_1_MW.jpg/2560px-Photovoltaik_Dachanlage_Hannover_-_Schwarze_Heide_-_1_MW.jpg" />
          </div>
        </div>
      </div>
    </div>
    )
  }
}

export default ProjectDetails