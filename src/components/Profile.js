import React, { Component } from 'react';
import { Fa, Button, Form, Input, Card, CardBody, CardTitle, CardText } from 'mdbreact';
import CountUp from 'react-countup';
import contractService from '../services/contractService'
import ProjectList from './ProjectList';

export default class Profile extends Component {
    constructor(props) {
       super(props)
        this.state = {
          account: "Loading...",
          accountBalance: "Loading...",
          tokenName: "EnexToken",
          tokenSymbol: "ENX",
          stakeAmount:0,
          liquidAmount:0,
        }
    this.updateValue.bind(this);
    this.getUser.bind(this);
    this.stakeTokens.bind(this);
    this.getBalance.bind(this);
    }

  componentDidMount() {

    //Get the balance
    this.getBalance();

    //Get user data
    this.getUser();

    //Initialize listener for blockhain transactions to update state
    window.web3.eth.filter('latest', (error, result) => {
      if (!error) {
        console.log(result)
        this.getBalance();
        this.getUser();
      } else {
        console.error(error)
      }
    });
  }

  componentWillUnmount() {
    //Remove listener
    window.web3.eth.filter('latest').stopWatching();
  }

  updateValue(event) {
    this.setState({ stakeAmount:Number(event.target.value) });
  }

  updateValue2(event) {
    this.setState({ liquidAmount:Number(event.target.value) });
  }

  // Get user details from the contract
  getUser() {
    window.web3.eth.getAccounts((error, accounts) => {
        this.setState({account:accounts[0]})
        contractService.getUser(accounts[0])
        .then((userObj) => {
          this.setState({userWeight:userObj.userWeight, userDivRatio:userObj.userDivRatio, userStaked:userObj.userStaked})
        })
        .catch((error) => {
          console.error(error)
        })
     })
  }

  // Get ENX balance from the contract
  getBalance() {
    contractService.getBalance()
    .then((accountBalance) => {
      this.setState({accountBalance:accountBalance})
    })
    .catch((error) => {
      console.error(error)
    })
  }

  //Function to stake tokens
  stakeTokens() {
    contractService.stakeTokens(this.state.stakeAmount)
        .then((transaction) => {
            //Update status
            console.log(transaction)
        })
        .catch((error) => {
          console.error(error)
          alert(error)
      })
  }

  //Function to liquidate tokens
  liquidateTokens() {
    contractService.liquidateTokens(this.state.liquidAmount)
        .then((transaction) => {
            //Update status
            console.log(transaction)
        })
        .catch((error) => {
          console.error(error)
          alert(error)
      })
  }

  //DEV: Request new tokens
  requestTokens() {
    contractService.requestTokens()
        .then((transaction) => {
            //Update status
            console.log(transaction)
        })
        .catch((error) => {
          console.error(error)
          alert(error)
      })
  }

  render() {
    return(
        <main>
            <div className="main-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 mt-1">
                          <Card className="profile-card">
                              <CardBody>
                                  <CardTitle><i className="fa fa-user enex-color-dark"/> Profile - {this.state.account}</CardTitle>
                                  <CardText>
                                            <h5>
                                            Balance:  <CountUp start={0} end={this.state.accountBalance} duration={0.5} />  {this.state.tokenSymbol}<br/>
                                            Staked tokens: <CountUp start={0} end={this.state.userStaked} duration={0.5} /> {this.state.tokenSymbol}
                                            </h5>
                                            <h5>
                                            Vote Weight: <CountUp start={0} end={this.state.userWeight} duration={0.5} /> <br/>
                                            Dividend ratio: <CountUp start={0} end={this.state.userDivRatio} duration={0.5} /> <br/>
                                         </h5>
                                            <br/>
                                  <Button color="orange" onClick={this.requestTokens.bind(this)}><Fa icon="exchange" className="mr-1"/>Request ENX</Button>
                                  </CardText>
                              </CardBody>
                          </Card>
                        </div>
                         <div className="col-md-4 mt-1">
                           <div className="row">
                              <div className="col-md-12">
                                <Card>
                                    <CardBody>
                                        <CardTitle><i className="fa fa-fire enex-color-dark"/> Spark up ENX</CardTitle>
                                        <CardText>
                                        <form className="mt-4">
                                            <Input label="Amount of tokens" name="amount" value={this.state.stakeAmount} onChange={evt => this.updateValue(evt)} type="number"/>
                                            <Button color="orange" onClick={this.stakeTokens.bind(this)}><Fa icon="exchange" className="mr-1"/>Stake ENX</Button>
                                        </form>
                                        </CardText>
                                    </CardBody>
                                </Card>
                              </div>
                            </div>
                            <div className="row mt-3">
                              <div className="col-md-12">
                                <Card>
                                    <CardBody>
                                        <CardTitle><i className="fa fa-tint enex-color-blue"/> Fizzle down ENX</CardTitle>
                                        <CardText>
                                        <form className="mt-4">
                                            <Input label="Amount of tokens" name="amount" value={this.state.liquidAmount} onChange={evt => this.updateValue2(evt)} type="number"/>
                                            <Button color="orange" onClick={this.liquidateTokens.bind(this)}><Fa icon="exchange" className="mr-1"/>Liquidate ENX</Button>
                                        </form>
                                        </CardText>
                                    </CardBody>
                                </Card>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        )
    }
}