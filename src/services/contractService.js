import EnexToken from '../../build/contracts/EnexToken.json'
var contract = require("truffle-contract");
var _ = require('lodash');

class ContractService {

  constructor(props, context) {
    this.tokenContract = contract(EnexToken)
  }

  //Get current Web3 injected account balance
  getBalance() {
    return new Promise((resolve, reject) => {
      this.tokenContract.setProvider(window.web3.currentProvider)
      window.web3.eth.getAccounts((error, accounts) => {
        this.tokenContract.deployed().then((instance) => {
          return instance.balanceOf(accounts[0]);
        }).then((result) => {
          let balance = result.c[0];
          resolve(balance)
        }).catch((error) => {
          console.error("Error" + error)
          reject(error)
        })
      })
    })
  }

  // Get user info for account
  getUser(account) {
    return new Promise((resolve, reject) => {
      this.tokenContract.setProvider(window.web3.currentProvider)

      this.tokenContract.deployed().then((instance) => {
        instance.getUser.call(account)
        .then((user)  => {
          const userObject = {
            userWeight: user[0].toNumber(),
            userDivRatio: user[1].toNumber(),
            userStaked:user[2].toNumber()
          }
          resolve(userObject)
        })
        .catch((error) => {
          console.log(`Error fetching account info for: ${account}`)
          reject(error)
        })
      })
    })
  }

  //Stake desired amount of tokens "spark up"
  stakeTokens(amount) {
    return new Promise((resolve, reject) => {
      this.tokenContract.setProvider(window.web3.currentProvider)
      window.web3.eth.getAccounts((error, accounts) => {
        this.tokenContract.deployed().then((instance) => {
          console.log(amount)
          instance.stakeTokens(
            amount,
            {from: accounts[0]}
          ).then((user) => {
            resolve(user)
          })
        })
      })
    })
  }

  //Liquidate desired amount of tokens "fizzle down up"
  liquidateTokens(amount) {
    return new Promise((resolve, reject) => {
      this.tokenContract.setProvider(window.web3.currentProvider)
      window.web3.eth.getAccounts((error, accounts) => {
        this.tokenContract.deployed().then((instance) => {
          console.log(amount)
          instance.liquidateTokens(
            amount,
            {from: accounts[0]}
          ).then((user) => {
            resolve(user)
          })
        })
      })
    })
  }

  //Vote for a project
  voteProject(index) {
    return new Promise((resolve, reject) => {
      this.tokenContract.setProvider(window.web3.currentProvider)
      window.web3.eth.getAccounts((error, accounts) => {
        this.tokenContract.deployed().then((instance) => {
          instance.voteProject(index, {from: accounts[0]}
          ).then((user) => {
            resolve(user)
          })
        })
      })
    })
  }

  //Mint new tokens for the current account
  requestTokens() {
    return new Promise((resolve, reject) => {
      this.tokenContract.setProvider(window.web3.currentProvider)
      window.web3.eth.getAccounts((error, accounts) => {
        this.tokenContract.deployed().then((instance) => {
          instance.requestTokens({from: accounts[0]}
          ).then((user) => {
            resolve(user)
          })
        })
      })
    })
  }

  //Get ids of projects in the pipeline
  getProjectIds() {
    return new Promise((resolve, reject) => {
      this.tokenContract.setProvider(window.web3.currentProvider)
      this.tokenContract.deployed().then((instance) => {
        instance.projectsLength.call().then((projectsLength) => {
          resolve(_.range(0, Number(projectsLength)))
        })
        .catch((error) => {
          console.log(`Can't get number of projects.`)
          reject(error)
        })
      })
      .catch((error) => {
        console.log(`Contract not deployed`)
        reject(error)
      })
    })
  }

  //Get info of a single project
  getProject(projectId) {
    return new Promise((resolve, reject) => {
      this.tokenContract.setProvider(window.web3.currentProvider)

      this.tokenContract.deployed().then((instance) => {
        instance.getProject.call(projectId)
        .then((project)  => {
          const projectObject = {
            id: project[0].toNumber(),
            name: project[1].toString(),
            votes: project[2].toNumber()
          }
          resolve(projectObject)
        })
        .catch((error) => {
          console.log(`Error fetching project info for: ${projectId}`)
          reject(error)
        })
      })
    })
  }

  //Read info from the Enex TokenContract
  getTokenInfo() {
    return new Promise((resolve, reject) => {
      this.tokenContract.setProvider(window.web3.currentProvider)
      window.web3.eth.getAccounts((error, accounts) => {
        this.tokenContract.deployed().then((instance) => {
          const tokenName = instance.name()
          resolve(tokenName);
        }).catch((error) => {
          console.error("Error" + error)
          reject(error)
        })
      })
    })
  }

}

const contractService = new ContractService()

export default contractService

