# Enex Solar Test app

Simple test dApp and Enex ERC20 token. It just gives the contract issuer all the tokens.

## How to install and run:

Required:
⋅⋅*NodeJS and NPM (latest is greatest)
⋅⋅*Truffle Ethereum development framework (npm install -g truffle)
⋅⋅*Local Ethereum test network - Ganache (recommended) or TestRPC
⋅⋅*MetaMask extension for Chrome/Firefox

First install Node packages
```npm install```

Make sure that Ganache testnet is running on port 8545

Compile and deploy Smart Contracts to your local testnet
```truffle compile```
```truffle migrate```

Start the development server
```npm start```

Wohoo, test server is up and running - navigate to localhost:3000

Make sure that you add your CustomRPC (Ganache testnet) to MetaMask (http://localhost:8545)
Unlock Ethereum accounts in MetaMask with by copying the corresponding private key from Ganache.

All the React/dApp code is at /src, the token contract at /contracts. (EnexToken.sol)



