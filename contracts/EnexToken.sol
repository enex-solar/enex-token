pragma solidity ^0.4.17;

import 'zeppelin-solidity/contracts/token/ERC20/StandardToken.sol';
import './Users.sol';

contract EnexToken is Users, StandardToken {

	string public name = 'EnexToken';
	string public symbol = 'ENX';
	uint8 public decimals = 2;
	uint public INITIAL_SUPPLY = 1000;

	struct ProjectStruct {
		string projectName; // name of a project
		uint votes; // votes of a project
	}


	ProjectStruct[] public projects;


	function EnexToken() public {
	  totalSupply_ = INITIAL_SUPPLY;
	  balances[msg.sender] = INITIAL_SUPPLY;
	  insertUser(msg.sender);
	  createProject("Test Project 1");
	  createProject("Test Project 2");
	}

	//Poc-funtion to request new tokens for testing purposes

	function requestTokens() public returns (bool) {
	    totalSupply_ = totalSupply_.add(1000);
	    balances[msg.sender] = balances[msg.sender].add(1000);
	    insertUser(msg.sender);
	    return true;
  	}

	modifier hasTokensToStake(uint amount) {
	   	require(amount <= balances[msg.sender]);
	    _;
  	}

  	modifier hasTokensToLiquidate(uint amount) {
	   	require(amount <= userStructs[msg.sender].stakedTokens);
	    _;
  	}


	function stakeTokens(
	   uint amount
	 )
	 public
	 hasTokensToStake(amount)
	 returns(bool)
	{
	 	userStructs[msg.sender].stakedTokens = userStructs[msg.sender].stakedTokens.add(amount);
		userStructs[msg.sender].weight += amount;
		userStructs[msg.sender].dividendRatio += amount;
		balances[msg.sender] = balances[msg.sender].sub(amount);
		return true;
	}

	function liquidateTokens(
	   uint amount
	 )
	 public
	 hasTokensToLiquidate(amount)
	 returns(bool)
	{
	 	userStructs[msg.sender].stakedTokens = userStructs[msg.sender].stakedTokens.sub(amount);
		userStructs[msg.sender].weight -= amount;
		userStructs[msg.sender].dividendRatio -= amount;
		balances[msg.sender] = balances[msg.sender].add(amount);
		return true;
	}

	function createProject(
		string projectName
	 )
	 public
	 returns(uint)
	{
		projects.push(ProjectStruct(projectName, 0));
	 	return projects.length;
	}

	function voteProject(
		uint index
	 )
	 public
	 returns(bool)
	{
		projects[index].votes+=(1 * userStructs[msg.sender].weight);
	 	return true;
	}

	function getProject(uint index)
    public
    constant
    returns (uint, string, uint)
  	{
	    return(
	      index,
	      projects[index].projectName,
	      projects[index].votes
	    );
  	}

    function projectsLength()
    public
    constant
    returns (uint)
  	{
      return projects.length;
  	}

}