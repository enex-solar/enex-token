pragma solidity ^0.4.17;

contract Users {

	struct UserStruct {
		uint weight; // voting weight is accumulated by staking tokens
		uint dividendRatio; // ratio is accumulated by staking tokens
		uint stakedTokens; // amount of staked tokens
	}


	mapping(address => UserStruct) userStructs;
	address[] userIndex;


	function insertUser(
	 address userAddress
	 )
	 public
	 returns(bool success)
	{
	 	userStructs[userAddress].weight=1000;
	 	userStructs[userAddress].dividendRatio=1000;
	 	userStructs[userAddress].stakedTokens=0;
	 	userIndex.push(userAddress);

	 	return true;
	}

	function getUser(address userAddress)
    public
    constant
    returns(uint userWeight, uint userDivRatio, uint stakedTokens)
  	{
    return(
      userStructs[userAddress].weight,
      userStructs[userAddress].dividendRatio,
      userStructs[userAddress].stakedTokens
      );
  	}

}